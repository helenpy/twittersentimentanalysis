#-*- coding: utf-8 -*-
import re
from string import letters, digits,punctuation
  
#  name: load_emoticons, load_slang, load_abreviaciones
#  @param ruta_emoticons, ruta_slangs, ruta_abreviaciones
#  @return modifica los diccionarios globales (emoticons, slangs y abreviatura)
#  Estas funciones cargan los diccionarios citados arriba.
######################################################################################################################
def load_emoticons(ruta_emoticons):                                                                                 #
    emoticons = {}#Diccionario de emoticons
    try:
        tmp = open(ruta_emoticons, "r")                                                                              #                                                                                                    #     
        while True :                                                                                                      #
            linea = tmp.readline ()                                                                                   #
            linea = to_unicode(linea) 
            if (not linea) or (linea == ""):                                                                               #
                break;                                                                                                      #
            linea = linea.rstrip()
            aux = linea.split("\t")                
            emoticons[aux[0]] = aux[1]
        return (emoticons)
    except IOError as e:
        print "Error: "+ruta_emoticons+" I/O error({0}): {1}".format(e.errno, e.strerror)
        exit(1)
    ################################################################
    

def load_slang(ruta_slangs):                                                                                        #
    slangs = {}#Diccionario de emoticons
    try:
        tmp = open(ruta_slangs, "r")                                                                              #                                                                                                    #     
        while True :                                                                                                      #
            linea = tmp.readline ()                                                                                   #
            linea = to_unicode(linea) 
            if (not linea) or (linea == ""):                                                                               #
                break;                                                                                                      #
            linea = linea.rstrip()
            aux = linea.split("\t")                
            slangs[aux[0]] = aux[1]
        return (slangs)
    except IOError as e:
        print "Error: "+ruta_slangs+" I/O error({0}): {1}".format(e.errno, e.strerror)
        exit(1)
    ################################################################
    

def load_abreviaturas(ruta_abreviaturas):                                                                         #
    abreviatura = {}#Diccionario de emoticons
    try:
        tmp = open(ruta_abreviaturas, "r")                                                                              #                                                                                                    #     
        while True :                                                                                                      #
            linea = tmp.readline ()                                                                                   #
            linea = to_unicode(linea) 
            if (not linea) or (linea == ""):                                                                               #
                break;                                                                                                      #
            linea = linea.rstrip()
            aux = linea.split("\t")        
            abreviatura[aux[0]]= aux[1]
        return (abreviatura)
    except IOError as e:
        print "Error: "+ruta_abreviaturas+" I/O error({0}): {1}".format(e.errno, e.strerror)
        exit(1)
    ################################################################ 

#######################################################################################################################
# Removes rare characters                                                                      #
#######################################################################################################################
def clean_text(texto):
    texto_limpio= texto.encode(encoding="UTF-8")
    texto_limpio=re.sub('\xE2\x80\x9C', '\x22', texto_limpio)
    texto_limpio=re.sub('\xE2\x80\x99', '\x27', texto_limpio)
    texto_limpio=re.sub('\xE2\x80\x98', '\x22', texto_limpio)
    texto_limpio=re.sub('\x22', ' \x22 ', texto_limpio)
    # Elimina comillas y comilla simple
    texto_limpio=re.sub('\x22', '', texto_limpio)
    texto_limpio=re.sub('\x27', '', texto_limpio)
    texto_limpio=re.sub('\xADh', '', texto_limpio)
    texto_limpio=re.sub('\xA8h', '', texto_limpio)
    texto_limpio=re.sub('\x09', '', texto_limpio)
    texto_limpio=re.sub('&', ' and ', texto_limpio)
    texto_limpio=re.sub('\x92', ' ', texto_limpio)
    texto_limpio=re.sub('u002c', ' ', texto_limpio)
    texto_limpio=to_unicode(texto_limpio)
    return texto_limpio
#######################################################################################################################
def remove_slang(texto,slangs):
    contSlang = 0        
    palabras = texto.split(" ")
    ntext = ""
    for palabra in palabras:
        if palabra.lower() in slangs.keys():
            ntext += slangs[palabra.lower()]+" "
            contSlang+=1
        else:
            ntext += palabra + " "
    ntext = ntext.rstrip()                
    return ntext,contSlang
#######################################################################################################################
def remove_abbreviation(texto,abreviaturas):
    contAbb = 0        
    palabras = texto.split(" ")
    ntext = ""
    for palabra in palabras:
        if palabra.lower() in abreviaturas.keys():
            ntext += abreviaturas[palabra.lower()]+" "
            contAbb+=1
        else:
            ntext += palabra + " "
    ntext = ntext.rstrip()                
    return ntext,contAbb    
#######################################################################################################################
def remove_retweet(texto):
    texto= texto.encode(encoding="UTF-8")
    contRetweet = 0
    texto=re.sub('í', 'i', texto)
    if texto.find('via') > 0:
        texto=texto[:texto.lower().find('via')]+texto[texto.lower().rfind('@username')+ len('@username '):]
        contRetweet+=1
    #if texto.find('vía') > 0:
    #    texto=texto[:texto.lower().find('vía')]+texto[texto.lower().rfind('@username')+ len('@username '):]
    #    contRetweet+=1
    texto= to_unicode(texto)
    return texto,contRetweet

    
######################################################################################################################
def remove_mention(texto):
    contRef = 0
    while texto.count('@username') > 0:
        ultimo=texto.lower().rfind('@username')
        aux=texto[ultimo+len('@username'):].strip(' ')
        ##solo si el @username está en primer lugar se elimina el username
        if ultimo == 0:
            texto=texto[len('@username')+1:]
        else: # en otro caso, se elimina la mension a usuario (@username)
            if len(aux) > 0:
                texto=texto[:ultimo]+' '+aux
            else:
                texto=texto[:ultimo]
        contRef+=1
    return texto,contRef   
    
#######################################################################################################################
def remove_hashtag(texto):
    contHash = 0
    while texto.rfind('#') > 0: 
        ultimo=texto.lower().rfind('#')
        longi=len(texto[ultimo:ultimo+texto[ultimo:].find(' ')])
        if longi == 0:
            longi=len(texto[ultimo:])
        aux=texto[ultimo+longi:].strip(' ')
        ##solo si el #tag esta en ultimo lugar se elimina toda la etiqueta, sino solo se elimina el simbolo #
        if len(aux) > 0:
            texto=texto[:ultimo-1]+' '+texto[ultimo+1:]
        else:
            texto=texto[:ultimo-1]
        contHash+=1
    ###Modif
    while texto.lower().find('#') > -1:
        texto = texto[texto.lower().find('#')+1:]
        
    return texto,contHash
    
#######################################################################################################################
#  name: remove_several                                                                                               #
#  @param texto                                                                                                       #
#  @return clean text                                                                                                 #
#  This function receives the original tweet, removes and count the emoticons, the urls, the hasgtags and retweets    #
#  Replace slangs, abreviations and  contractions for their meaning                                                   #
#  and keep the countings                                                                                             #
#######################################################################################################################
def remove_several(texto,slangs,emoticons, abreviaturas):
    contUrl = 0
    contEmo = 0
    contPos = 0
    Emo = {}
    indice = 9999999
    palabra=""
    simbolo=""
    encontrados=set()
    indicesHttp=set()
    texto=clean_text(texto)
    texto,contSlang     = remove_slang(texto,slangs)
    texto,contAbb       = remove_abbreviation(texto,abreviaturas)
    texto,contRetweet   = remove_retweet(texto)
    texto,contRef       = remove_mention(texto)
    
    #print texto
    for t in texto:
        if t in letters:
            palabra = palabra + t
            simbolo = simbolo.strip(" ")
            if len(simbolo) > 0:
                setSimbolos=simbolo.split(" ")
                for s in setSimbolos:
                    s = s.strip(" ")
                    if s in emoticons:
                        encontrados.add(s)
                        if Emo.has_key(s):
                            Emo[s] += 1
                        else:
                            Emo[s] = 1
            simbolo=""
        else:
            simbolo = simbolo + t
            if palabra[:4] == 'http':
                indicesHttp.add(contPos-len(palabra)+1)
            palabra = ""
        contPos+=1
         
    ##proceso final#########
    if palabra[:4] == 'http':
        #print contPos
        indicesHttp.add(contPos-len(palabra)+1)
    simbolo = simbolo.strip(" ")
    if len(simbolo) >0:
        setSimbolos=simbolo.split(" ")
        for s in setSimbolos:
            if s in emoticons:
                encontrados.add(s)
                if Emo.has_key(s):
                    Emo[s] += 1
                else:
                    Emo[s] = 1

    ## aqui se eliminan los emoticons encontrados en el texto y se cuentan
    if len(encontrados) > 0:
        for e in encontrados:
            texto=texto[:texto.find(e)]+texto[texto.find(e)+len(e):]
            contEmo+=1
    ## aqui se elimina la url encontrada en el texto; si la url va en el medio de la frase, se extrae solo esa parte,
    ## si va al final de la frase se extrae la parte final. 
    for i in indicesHttp:
        indice = texto.find("http")
        if texto[indice:].find(" ") > 0:
            texto=texto[:indice]+texto[indice+texto[indice:].find(" "):]
        else:
            texto=texto[:indice]
        contUrl+=1
    texto,contHash = remove_hashtag(texto)
    #Added by JP#    
    #print "linea: "+texto    
    txt =""
    if len(texto) > 0:
        while texto[0] not in punctuation and texto[0] not in letters and texto[0] not in digits:
            texto = texto[1:]
            if len(texto) < 1:
                break             
        for item in texto.split(" "):
            if len(item) > 0: 
                if item[0] in letters or item[0] in digits:
                    txt += item + " "
        txt = txt.rstrip()                   
    return (txt,[contUrl,contEmo,Emo,contHash,contSlang,contRef,contRetweet,contAbb])
   
##############################################################################################################################
#  
#  name: preprocess
#  @param ifile, tipo=(TRAIN, TEST or UNLABELED), sep, posTweet, posClass
#  @return preprocessed file
#
##############################################################################################################################  
def to_unicode(obj):        
        encoding = 'UTF-8'
        if isinstance(obj, basestring):
            if not isinstance(obj, unicode):
                obj = unicode(obj, encoding,'replace')
        return obj
    
def preprocess(ifile, tipo, sep, posTweet, posClass):
    ruta_emoticons='EN/emoticons.txt'
    ruta_abreviaturas='EN/ENabb.txt'
    ruta_slangs='EN/ENslang.txt'
    emoticons   = load_emoticons(ruta_emoticons)
    abreviaturas = load_abreviaturas(ruta_abreviaturas)
    slangs      = load_slang(ruta_slangs)
    contUrl=0          # URLs counter
    contEmo=0          # emoticons counter
    Emo = {}
    contSlang=0        # slangs counter
    contHash=0         # hash tags counter
    contRef=0          # reference counter
    contRetweet=0      # retweet counter
    ofile   = ""       # preprocessed output 
    try:
        archivo = open(ifile, "r")
    except IOError as e:
        print "Error: "+ ifile +" I/O error({0}): {1}".format(e.errno, e.strerror)
        exit(1)
    cont = 0
    for linea in archivo.readlines():
        cont += 1
        contUrl=0          # URLs counter
        contEmo=0          # emoticons counter
        Emo = {}
        contSlang=0        # slangs counter
        contHash=0         # hash tags counter
        contRef=0          # reference counter
        contRetweet=0      # retweet counter
        linea = to_unicode(linea)
        lineaSep = linea.split(sep)
        tweet = lineaSep[posTweet].strip('\n')
        clase = lineaSep[posClass].strip('"')
        if clase != 'objective':
            linea,values = remove_several(tweet,slangs,emoticons, abreviaturas)                
            contUrl     += values[0]
            contEmo     += values[1]
            contHash    += values[3]
            contSlang   += values[4]
            contRef     += values[5]
            contRetweet += values[6]
            for i in values[2].keys():
                if Emo.has_key(i):
                    Emo[i]+=1
                else:
                    Emo[i]=1
            emoticones=" ".join(values[2].keys())
            if clase not in ('positive', 'negative', 'neutral') and tipo=='TRAIN':
                clase='neutral'
            if tipo == 'TRAIN':
                linea += "|"+clase+"|"+str(contUrl)+"|"+str(contEmo)+"|"+str(contHash)+"|"+str(contSlang)+"|"+str(contRef)+"|"+str(contRetweet)+"|"+emoticones+"\n"
            else:
                linea += "|"+clase+"|"+str(contUrl)+"|"+str(contEmo)+"|"+str(contHash)+"|"+str(contSlang)+"|"+str(contRef)+"|"+str(contRetweet)+"|"+emoticones+"\n" 
            
            ofile =ofile+linea.encode(encoding="UTF-8")
    ofile=ofile.strip('\n')
    archivo.close()
    return ofile

def preprocessUnlab(ifile):
    ruta_emoticons='EN/emoticons.txt'
    ruta_abreviaturas='EN/ENabb.txt'
    ruta_slangs='EN/ENslang.txt'
    emoticons   = load_emoticons(ruta_emoticons)
    abreviaturas = load_abreviaturas(ruta_abreviaturas)
    slangs      = load_slang(ruta_slangs)
    ofile   = ""       # preprocessed output 
    try:
        archivo = open(ifile, "r")
    except IOError as e:
        print "Error: "+ ifile +" I/O error({0}): {1}".format(e.errno, e.strerror)
        exit(1)
    for linea in archivo.readlines():
        linea = to_unicode(linea)
        tweet = linea
        linea, values = remove_several(tweet,slangs,emoticons, abreviaturas)
        ofile =ofile + linea.encode(encoding="UTF-8")+'\n'
    ofile=ofile.strip('\n')
    archivo.close()
    return ofile