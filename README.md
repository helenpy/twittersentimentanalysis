# Project Name #
Sentiment Analysis in Twitter (Semeval 2016 Task 4-A)

# Description #
The aim of the system is to classify Twitter messages into positive, neutral and negative polarity. We used a lexical resource for pre-processing of social media data and train a neural network model for feature representation. Our resource includes dictionaries of slang words, contractions, abbreviations, and emoticons commonly used in social media. For the classification process, we introduce the features obtained in an unsupervised manner into an SVM classifier.

# Python Requirements #

* Numpy
* Scikit-learn
* Gensim
* NLTK

# Members #

* Helena Gómez Adorno
* Darnes Vilariño
* David Pinto
* Grigori Sidorov