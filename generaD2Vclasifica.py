import random
# gensim modules
from gensim.models.doc2vec import LabeledSentence
from gensim.models import Doc2Vec
#sklearn modules
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.preprocessing import normalize
import numpy as np

def labelize(reviews, label_type):    
    labelized = []
    for i,v in enumerate(reviews):
        label = '%s_%s'%(label_type,i)
        labelized.append(LabeledSentence(v, [label]))
    return labelized
    
def getVecs(model,corpus,size):
    vecs = [np.array(model.docvecs[z[1]]).reshape((1,size)) for z in corpus]
    return np.concatenate(vecs)

def d2v_classify(trainFile, testFile, unlabeledFile, OutputPath):
    ofile = open(OutputPath,'w')
    #oauxfile = open(OutputPath+".aux",'w')
    sentimientos=["negative","neutral","positive"]
    print "Reading Train"
    
    ###Read Train####
    target  = np.array([-1],float)    
    lines   = []#Variable that contains all training tweets 
    labels  = []#Variable that keeps track of the number of tweets each class
    for text in trainFile.split('\n'):
        texto= text.split("|")
        clase = texto[1]
        if clase in ["positive","negative","neutral"]:
            doc=texto[0]
            lines.append(doc)        
            aux = np.ones(1,float)
            aux = sentimientos.index(clase)*aux
            target = np.concatenate((target,aux))
            labels.append(clase)            
    target = target[1:]
         
    lines = labelize(lines, 'TRAIN')
    print len(target)
    
    ###Read Test###
    print "Reading Test"
    
    tlines   = []#Variable that contains all test tweets 
    for text in testFile.split('\n'):
        texto= text.split("|")
        doc=texto[0]
        tlines.append(doc)        
    tlines = labelize(tlines, 'TEST')
    
    ulines  = []#Variable that contains all unlabeled tweets
    for text in unlabeledFile.split('\n'):
        doc=text
        ulines.append(doc)        
    ulines = labelize(ulines, 'UNLABELED')
    
    model = Doc2Vec(min_count=2, window=3, size=300, sample=1e-5, negative=5, workers=7)
    
    aux= list(lines)
    aux.extend(tlines)
    aux.extend(ulines)
    model.build_vocab(aux)
    docs = range(0,len(aux))
    
    print "Training D2V model"
    for epoch in range(10):
        random.shuffle(docs)
        all_docs = [aux[item] for item in docs]
        model.train(all_docs)
    
    train_vecs = getVecs(model,lines,300)
    test_vecs = getVecs(model,tlines,300)
    
    print "Training SVM model"
    liblinear = LinearSVC() 
    Xn = normalize(train_vecs,norm='l2')
    liblinear.fit(Xn, target)
    Xxn = normalize(test_vecs,norm='l2')
    pre = liblinear.predict(Xxn)
    
    i = 1
    
    clases={0:"negative",1:"neutral",2:"positive"}
    for p in pre:
        ofile.write(str(i)+"\t"+clases[p]+"\n")
        i+=1
    
    ofile.close()
