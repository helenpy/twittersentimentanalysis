'''
Created on 26/02/2016

@author: Helena-Gomez
'''
from preprocessing import preprocess
from preprocessing import preprocessUnlab
from generaD2Vclasifica import d2v_classify
import os

if __name__ == '__main__':
	#Path of the training, unlabeled and test sets.
    unlabeledFile=''
    trainFile=''
    testfile=''
    
    #Path of the preprocesed files
    unlabeledFilePre='unlabeled.pre.txt'
    trainFilePre='train.pre.txt'
    testFilePre='test.pre.txt'
    
    #files separation character
    sep='\t'
    
    #preprocessing the files if they do not exists
    if not os.path.exists(trainFilePre):
        print "Preprocessing train"
        trainPre=preprocess(trainFile, 'TRAIN', sep, posTweet=3, posClass=2)
        tempFile=open(trainFilePre,'w')
        tempFile.write(trainPre)
        tempFile.close()
    else:
        trainPre=open(trainFilePre,'r').read()
        
    if not os.path.exists(testFilePre):
        print "Preprocessing test"
        testPre=preprocess(testfile, 'TEST', sep, posTweet=2, posClass=1)
        tempFile=open(testFilePre,'w')
        tempFile.write(testPre)
        tempFile.close()
    else:
        testPre=open(testFilePre,'r').read()
        
    if not os.path.exists(unlabeledFilePre):
        print "Preprocessing unlabeled"
        unlabeledPre=preprocessUnlab(unlabeledFile)
        tempFile=open(unlabeledFilePre,'w')
        tempFile.write(unlabeledPre)
        tempFile.close()
    else:
        unlabeledPre=open(unlabeledFilePre,'r').read()
    
    print "Train: ", len(trainPre.split('\n'))
        
    print "Test: ", len(testPre.split('\n'))
    print "Unlabeled: ", len(unlabeledPre.split('\n'))
    
    #Output path
    ofilePath='salidaTest2016.txt'
    d2v_classify(trainPre, testPre, unlabeledPre, ofilePath)
    print "Finished"
